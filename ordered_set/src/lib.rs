use std::slice::Iter;
use std::vec;

pub struct OrderedSet<T>
    where T: Ord
{
    items: Vec<T>,
}

impl<T> OrderedSet<T> 
    where T: Ord
{
    pub fn new() -> OrderedSet<T> {
        OrderedSet { items: Vec::new()}
    }

    pub fn insert(&mut self, item: T) {
        if let Err(index) = self.items.binary_search(&item) {
            self.items.insert(index, item);
        }
    }

    pub fn remove(&mut self, item: T) -> T {
        match self.items.binary_search(&item) {
            Ok(index) => self.items.remove(index),
            Err(_) => panic!("index out of bounds")
        }
    }

    pub fn has(&self, item: T) -> bool {
        match self.items.binary_search(&item) {
            Ok(_) => true,
            Err(_) => false
        }
    }

    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn iter(&self) -> Iter<T> {
        self.items.iter()
    }
}

impl<T> OrderedSet<T> 
    where T: Ord + ToString
{
    pub fn to_string(&self) -> String {
        let len = self.items.len();
        let mut s = String::new();

        if len == 0 {
            return s
        }

        s.push_str(&self.items[0].to_string());
        for i in 1..len {
            s.push_str(" ");
            s.push_str(&self.items[i].to_string());
        }
        s
    }
}

impl<T> IntoIterator for OrderedSet<T>
    where T: Ord
{
    type Item = T;
    type IntoIter = vec::IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        self.items.into_iter()
    }
}

impl<'a, T> IntoIterator for &'a OrderedSet<T>
    where T: Ord
{
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        self.items.iter()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert() {
        let mut set = OrderedSet::new();
        set.insert(3);
        set.insert(1);
        set.insert(2);

        assert_eq!("1 2 3", set.to_string());
    }

    #[test]
    fn remove() {
        let mut set = OrderedSet::new();
        set.insert(3);
        set.insert(1);
        set.insert(2);
        set.remove(2);

        assert_eq!("1 3", set.to_string());
    }

    #[test]
    fn has() {
        let mut set = OrderedSet::new();
        set.insert(3);

        assert!(set.has(3));
    }

    #[test]
    fn has_not() {
        let mut set = OrderedSet::new();
        set.insert(3);

        assert!(!set.has(2));
    }

    #[test]
    fn iter() {
        let mut set = OrderedSet::new();
        set.insert(3);
        set.insert(1);
        set.insert(2);

        let mut iter = set.iter();

        assert_eq!(Some(&1), iter.next());
        assert_eq!(Some(&2), iter.next());
        assert_eq!(Some(&3), iter.next());
        assert_eq!(None, iter.next());
    }
}
