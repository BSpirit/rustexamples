use json_parser::*;
use std::collections::HashMap;

fn main() {
    let mut map = HashMap::new();
    map.insert("First name", JSONvalue::String("Tony"));
    map.insert("Age", JSONvalue::Number(28));
    map.insert("Test", JSONvalue::List(vec![JSONvalue::String("first"), JSONvalue::Number(20), JSONvalue::Boolean(true)]));

    let mut map2 = HashMap::new();
    map2.insert("First name", JSONvalue::String("Tony"));
    map2.insert("Age", JSONvalue::Number(28));
    map2.insert("Test", JSONvalue::Object(map));

    let json = JSONvalue::Object(map2);
    println!("{}", json);
}
