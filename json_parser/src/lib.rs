use std::vec::Vec;
use std::collections::HashMap;
use std::fmt;


pub enum JSONvalue {
    String(&'static str),
    Number(u32),
    Boolean(bool),
    List(Vec<JSONvalue>),
    Object(HashMap<&'static str, JSONvalue>)
}

impl fmt::Display for JSONvalue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.print(f, 0)
    }
}

impl JSONvalue {
    fn print(&self, f: &mut fmt::Formatter, tab: u32) -> fmt::Result {
        match self {
            JSONvalue::String(s) => write!(f, "\"{}\"", s),
            JSONvalue::Number(n) => write!(f, "{}", n),
            JSONvalue::Boolean(b) => write!(f, "{}", b),
            JSONvalue::List(l) => print_list(l, f, tab),
            JSONvalue::Object(o) => print_object(o, f, tab),
        }
    }
}

fn print_list(l: &Vec<JSONvalue>, f: &mut fmt::Formatter, tab: u32) -> fmt::Result {
    let mut it = l.iter();
    write!(f, "[")?;
    if let Some(val) = it.next() {
        write!(f, " ")?;
        val.print(f, tab)?;
    }
    while let Some(val) = it.next() {
        write!(f, ", ")?;
        val.print(f, tab)?;
    }
    write!(f, " ]")
}

fn print_object(o: &HashMap<&'static str, JSONvalue>, f: &mut fmt::Formatter, tab: u32) -> fmt::Result {
    let mut it = o.iter();
    write!(f, "{{")?;
    if let Some(val) = it.next() {
        write!(f, "\n")?;
        print_tab(f, tab + 1)?;
        write!(f, "\"{}\" : ", val.0)?;
        val.1.print(f, tab + 1)?;
    }
    while let Some(val) = it.next() {
        write!(f, ",\n")?;
        print_tab(f, tab + 1)?;
        write!(f, "\"{}\" : ", val.0)?;
        val.1.print(f, tab + 1)?;
    }
    write!(f, "\n")?;
    print_tab(f, tab)?;
    write!(f, "}}")
}

fn print_tab(f: &mut fmt::Formatter, tab: u32) -> fmt::Result{
    for _ in 0..tab {
        write!(f, "    ")?;
    }
    Ok(())
}
